public class HourlyEmployee extends StaffMember {

    //fields

    private int hoursWorked;
    private double rate;
    private double payment;

    //get set

    public int getHoursWorked(){
        return hoursWorked;
    }
    public void setHoursWorked(int h){
        hoursWorked = h;
    }
    public double getRate(){
        return rate;
    }
    public void setRate(double r){
        rate = r;
    }
    public double getPayment(){
        return pay();
    }
    public void setPayment(double p){
        payment = p;
    }

    HourlyEmployee(int id,String name,String address,int hoursWorked,double rate){
        super(id,name,address);
        this.hoursWorked = hoursWorked;
        this.rate = rate;
    }
    public void ToString(){
        System.out.println("ID : "+ getId());
        System.out.println("Name : "+ getName());
        System.out.println("Address : "+ getAddress());
        System.out.println("Hours Worked : "+ getHoursWorked());
        System.out.println("Rate : "+ getRate());
        System.out.println("Payment : "+ getPayment());
        System.out.println("--------------------------------------------");
    }
    public double pay(){
        return payment = hoursWorked * rate;
    }
}
