public class SalariedEmployee extends StaffMember {

    //fields

    private double salary;
    private double bonus;
    private double payment;

    //get set

    public double getSalary(){
        return salary;
    }
    public void setSalary(double s){
        salary = s;
    }
    public double getBonus(){
        return bonus;
    }
    public void setBonus(double b){
        bonus = b;
    }
    public double getPayment(){
        return pay();
    }
    public void setPayment(double p){
        payment = p;
    }

    SalariedEmployee(int id,String name,String address,double salary,double bonus){
        super(id,name,address);
        this.salary = salary;
        this.bonus = bonus;
    }
    public void ToString(){
        System.out.println("ID : "+ getId());
        System.out.println("Name : "+ getName());
        System.out.println("Address : "+ getAddress());
        System.out.println("Salary : "+ getSalary());
        System.out.println("Bonus : "+ getBonus());
        System.out.println("Payment : "+ getPayment());
        System.out.println("--------------------------------------------");
    }
    public double pay(){
        return payment = salary + bonus;
    }
}
