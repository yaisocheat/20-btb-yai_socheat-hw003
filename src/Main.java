import java.net.Inet4Address;
import java.util.*;
import java.util.regex.Pattern;

public class Main {
    Scanner sc = new Scanner(System.in);
    ArrayList<StaffMember> sm;
    int option, valiId,valiHour,remid,editid;
    double valiRate,valiSalary,valiBonus;
    String valiName, valiAddress;

    Main() {
        sm = new ArrayList<StaffMember>();

        //add elements

        sm.add(new Volunteer(1, "Luffy", "Tokyo"));
        sm.add(new SalariedEmployee(2, "Zoro", "Tokyo", 2000, 200));
        sm.add(new HourlyEmployee(3, "Sanji", "Tokyo", 9, 30.0));

    }

    public static Comparator<StaffMember> sNameComparator = new Comparator<StaffMember>() {
        @Override
        public int compare(StaffMember s1, StaffMember s2) {
            String stuName1 = s1.name.toUpperCase();
            String stuName2 = s2.name.toUpperCase();

            return stuName1.compareTo(stuName2);
        }
    };
    public void show(){
        Collections.sort(sm,Main.sNameComparator);
        for (StaffMember i: sm){
            System.out.println();
            i.ToString();
            System.out.println("==============================");
        }
    }
    public boolean reg(String s){
        boolean result = Pattern.matches("\\d",s);
        return result;
    }
    public boolean reg1(String s) {
        return Pattern.matches("\\D+", s);
    }
    public boolean reg2(String s){
        boolean result = Pattern.matches("\\d+",s);
        return result;
    }
    public int option(){
        System.out.print("Choose option(1-4) : ");
        String opt = sc.nextLine();
        if (reg(opt)){
            int o = Integer.parseInt(opt);
            if (o<0 || o>4){
                option();
            }else {
                option = o;
            }
        }else {
            option();
        }
        return option;
    }
    public int valiId(){
        System.out.print("Enter Staff Member's ID       : ");
        String vid = sc.nextLine();
        if (reg(vid)){
            valiId = Integer.parseInt(vid);
        }else {
            valiId();
        }
        return valiId;
    }
    public String valiName(){
        System.out.print("Enter Staff Member's Name     : ");
        String vn = sc.nextLine();
        if (reg1(vn)){
            valiName = vn;
        }else {
            valiName();
        }
        return valiName;
    }
    public String valiAddress(){
        System.out.print("Enter Staff Member's Address  : ");
        String va = sc.nextLine();
        if (reg1(va)){
            valiAddress = va;
        }else {
            valiAddress();
        }
        return valiAddress;
    }
    public int valiHour(){
        System.out.print("Enter Hours Worked            : ");
        String vh = sc.nextLine();
        if (reg(vh)){
            valiHour = Integer.parseInt(vh);
        }else {
            valiHour();
        }
        return valiHour;
    }
    public double valiRate(){
        System.out.print("Enter Rate                    : ");
        String vr = sc.nextLine();
        if (reg2(vr)){
            valiRate = Double.parseDouble(vr);
        }else {
            valiRate();
        }
        return valiRate;
    }
    public double valiSalary(){
        System.out.print("Enter Staff's Salary          : ");
        String vs = sc.nextLine();
        if (reg2(vs)){
            valiSalary = Double.parseDouble(vs);
        }else {
            valiSalary();
        }
        return valiSalary;
    }
    public double valiBonus(){
        System.out.print("Enter Staff's Bonus           : ");
        String vb = sc.nextLine();
        if (reg2(vb)){
            valiBonus = Double.parseDouble(vb);
        }else {
            valiBonus();
        }
        return valiBonus;
    }
    public int remId(){
        System.out.print("Enter Employee ID to Remove   : ");
        String ri = sc.nextLine();
        if (reg(ri)){
            remid = Integer.parseInt(ri);
        }else {
            remId();
        }
        return remid;
    }
    public int editId(){
        System.out.print("Enter Employee ID to Update   : ");
        String ei = sc.nextLine();
        if (reg(ei)){
            editid = Integer.parseInt(ei);
        }else {
            editId();
        }
        return editid;
    }
    public void edit(){
        int count = 0;
        System.out.println("======== EDIT INFO ========");
        editid = editId();
        for (StaffMember s : sm){
            if (s.id == editid){
                count++;
            }
        }
        if (count != 0) {
            Iterator i = sm.iterator();
            while (i.hasNext()) {
                StaffMember smi = (StaffMember) i.next();
                if (smi.id == editid) {
                    System.out.println();
                    smi.ToString();
                    System.out.println();
                    i.remove();
                    if (smi instanceof SalariedEmployee) {
                        String name = valiName();
                        String address = valiAddress();
                        double salary = valiSalary();
                        double bonus = valiBonus();
                        sm.add(new SalariedEmployee(editid, name, address, salary, bonus));
                    } else if (smi instanceof HourlyEmployee) {
                        String name = valiName();
                        String address = valiAddress();
                        int hour = valiHour();
                        double rate = valiRate();
                        sm.add(new HourlyEmployee(editid, name, address, hour, rate));
                    } else if (smi instanceof Volunteer) {
                        String name = valiName();
                        String address = valiAddress();
                        sm.add(new Volunteer(editid, name, address));
                    }
                    System.out.println();
                    System.out.println("Edit Successfully!!!");
                    show();
                    Menu1();
                }
            }
        }else {
            System.out.println();
            System.err.println("Id not found!!! can not edit Employee!!!");
            System.out.println();
            edit();
        }
    }
    public void remove(){
        int count = 0;
        System.out.println("======== DELETE ========");
        remid = remId();
        for (StaffMember s : sm){
            if (s.id == remid){
                count++;
            }
        }
        if (count != 0){
            Iterator i = sm.iterator();
            while (i.hasNext()){
                StaffMember smi = (StaffMember) i.next();
                if (smi.id == remid){
                    i.remove();
                    smi.ToString();
                    System.out.println("");
                    System.out.println("Removed Successfully!!!");
                    System.out.println();
                    show();
                    Menu1();
                }
            }
        }else {
            System.out.println();
            System.err.println("Id not found!!! can not remove Employee!!!");
            System.out.println();
            remove();
        }
    }
    public void addVolunteer(){
        int smid;
        String smname,smaddress;
        System.out.println("======== INSERT INFO ========");
        smid = valiId();
        smname = valiName();
        smaddress = valiAddress();
        sm.add(new Volunteer(smid,smname,smaddress));
        System.out.println("--------------------------------------------");
    }
    public void addHourlyEmp(){
        int smid,smhour;
        double smrate;
        String smname,smaddress;
        System.out.println("======== INSERT INFO ========");
        smid = valiId();
        smname = valiName();
        smaddress = valiAddress();
        smhour = valiHour();
        smrate = valiRate();
        sm.add(new HourlyEmployee(smid,smname,smaddress,smhour,smrate));
        System.out.println("--------------------------------------------");
    }
    public void addSalariedEmp(){
        int smid;
        double smsalary,smbonus;
        String smname,smaddress;
        System.out.println("======== INSERT INFO ========");
        smid = valiId();
        smname = valiName();
        smaddress = valiAddress();
        smsalary = valiSalary();
        smbonus = valiBonus();
        sm.add(new SalariedEmployee(smid,smname,smaddress,smsalary,smbonus));
        System.out.println("--------------------------------------------");
    }
    public void Menu1(){
        System.out.println("--------------------------------------------");
        System.out.println("1). Add Employee");
        System.out.println("2). Edit");
        System.out.println("3). Remove");
        System.out.println("4). Exit");
        int op = option();
        switch (op){
            case 1: Menu2();
                    break;
            case 2: edit();
                    break;
            case 3: remove();
                    break;
            case 4: System.out.println("(^-^) Good Bye! (^-^)");
                    System.exit(0);
                    break;
        }
    }
    public void Menu2(){
        System.out.println("--------------------------------------------");
        System.out.println("1). Volunteer");
        System.out.println("2). Hourly Emp");
        System.out.println("3). Salaried Emp");
        System.out.println("4). Back");
        int op = option();
        switch (op){
            case 1: addVolunteer();
                    break;
            case 2: addHourlyEmp();
                    break;
            case 3: addSalariedEmp();
                    break;
            case 4: Menu1();
                    break;
        }
        show();
        Menu1();
    }
    public static void main(String[] args) {
        Main m = new Main();
        m.show();
        m.Menu1();
    }
}
