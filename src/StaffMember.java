public abstract class StaffMember {

    //fields

    protected int id;
    protected String name;
    protected String address;

    //read and write

    public int getId(){
        return id;
    }
    public void setId(int i){
        id = i;
    }
    public String getName(){
        return name;
    }
    public void setName(String n){
        name = n;
    }
    public String getAddress(){
        return address;
    }
    public void setAddress(String a){
        address = a;
    }
    StaffMember(int i,String n,String a){
        id = i;
        name = n;
        address = a;
    }
    public void ToString(){
        System.out.println("ID : "+ getId());
        System.out.println("Name : "+ getName());
        System.out.println("Address : "+ getName());
        System.out.println("--------------------------------------------");
    }
    public abstract double pay();
}
