public class Volunteer extends StaffMember {
    Volunteer(int id,String name,String address){
        super(id,name,address);
    }
    public void ToString(){
        System.out.println("ID : "+ getId());
        System.out.println("Name : "+ getName());
        System.out.println("Address : "+ getAddress());
        System.out.println("Thanks!");
        System.out.println("--------------------------------------------");
    }
    public double pay(){
        return 0.0;
    }
}
